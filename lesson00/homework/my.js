/* eslint-disable */
'use strict';

let lodashScript = false;
let modulesCounter = 0;
const modulesRequested = 3;
let myScript = false;
let studentScript = false;

for (let i = 0; i < document.scripts.length; i++) {
  if (document.scripts[i].outerHTML.indexOf('your.js') >= 0) {
    if (typeof(variable) !== 'undefined') {
      studentScript = true;
      ++modulesCounter;
      alert(`Your module is here! That's ${modulesCounter}`);
    }
  } else if (document.scripts[i].outerHTML.indexOf('lodash.min.js') >= 0) {
    if (typeof(_) !== 'undefined') {
      lodashScript = true;
      ++modulesCounter;
      alert(`Lodash is here! That's ${modulesCounter}`);
    }
  } else if (document.scripts[i].outerHTML.indexOf('myAwesomeLibraryWithLongName.js') >= 0) {
    if (typeof(lib) !== 'undefined') {
      myScript = true;
      ++modulesCounter;
      alert(`My awesome module is here! That's ${modulesCounter}`);
    }
  }
}

if (modulesCounter === modulesRequested) {
  alert('Congratulations, all modules has been installed!');
  alert('<-- The pass phrase is');

  const alph = 'abcdefghijklmnopqrstuvwxyz';
  const encryptedPassPhrase = 'fjxirjybogxzhxkafjlhfpibbmxiikfdeqxkaftlohxiiaxv';
  let decryptedPassPhrase = '';
  const CESAR_SHIFT = parseInt(11, 2);
  const phraseLength = parseInt(110000, 2);

  for (let i = 0; i < phraseLength; i++) {
    let letter = encryptedPassPhrase[i];
    let index = alph.indexOf(letter) + CESAR_SHIFT;
    decryptedPassPhrase += index >= alph.length ? alph[index - alph.length] : alph[index];
  }
  document.querySelector('.passphrase').innerText = decryptedPassPhrase;
} else {
  document.scripts.length > 4
  ? alert(`Don't cheat, only ${modulesRequested} modules allowed!`)
  : alert(`Sorry, modules installed ${modulesCounter}. But it should be ${modulesRequested}.`);
  
}
