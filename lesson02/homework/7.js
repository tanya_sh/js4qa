let answer = Number(prompt('Enter a number from 0 to 100', '0'));
while (answer > 100 || answer < 0) {
  answer = Number(prompt('Enter a number from 0 to 100', '0'));
}
let i = 0;
let answer1;
let indicator = false;
while (i < 5 && !indicator) {
  answer1 = Number(prompt('Guess a number', '0'));
  if (answer1 > answer) {
    alert('Less');
  }
  if (answer1 < answer) {
    alert('More');
  }
  if (answer1 === answer) {
    alert('You are right!');
    indicator = true;
  }
  i += 1;
}
if (indicator === false) {
  alert('Dont worry!');
}
