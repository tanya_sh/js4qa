let answer = Number(prompt('Enter a number from -100 to 100', '0'));
while (answer > 100 || answer < -100) {
  answer = Number(prompt('Enter a number from -100 to 100', '0'));
}
console.log(answer);
if (answer > 0) {
  while (answer >= 0) {
    console.log(answer);
    answer -= 1;
  }
} else {
  while (answer <= 0) {
    console.log(answer);
    answer += 1;
  }
}
